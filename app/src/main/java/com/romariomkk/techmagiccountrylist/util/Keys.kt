package com.romariomkk.techmagiccountrylist.util

import android.content.Context
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri


class Keys {

    companion object {

        const val COUNTRY_LIST_ITEM = "COUNTRY_LIST_ITEM"

        const val EMPTY_PREVIOUS_REQUEST = "EMPTY_PREVIOUS_REQUEST"

        @JvmStatic
        fun openInBrowser(context: Context, url: String) {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(browserIntent)
        }
    }
}
package com.romariomkk.techmagiccountrylist.view.base

interface IFilterableAdapter<W, H> {

    val unfilteredData: List<W>

    fun filter(constraint: H) {
        filter(unfilteredData, constraint)
    }

    private fun filter(initialData: List<W>, constraint: H) {
        if (initialData.isNotEmpty()) {
            publishResults(initialData.filter { item -> isRuleActivated(item, constraint) })
        } else {
            publishResults(initialData)
        }
    }

    fun isRuleActivated(whatToCompare: W, withWhatToCompare: H): Boolean

    fun publishResults(newItems: List<W>)

}
package com.romariomkk.techmagiccountrylist.view.main

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.romariomkk.techmagiccountrylist.R
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import com.romariomkk.techmagiccountrylist.databinding.ActivityMainBinding
import com.romariomkk.techmagiccountrylist.util.Keys
import com.romariomkk.techmagiccountrylist.util.Resource
import com.romariomkk.techmagiccountrylist.util.annotation.RequiresView
import com.romariomkk.techmagiccountrylist.util.annotation.RequiresViewModel
import com.romariomkk.techmagiccountrylist.view.base.AbsActivity
import com.romariomkk.techmagiccountrylist.view.base.OnItemClickListener
import kotlinx.android.synthetic.main.activity_main.*


@RequiresView(R.layout.activity_main)
@RequiresViewModel(MainViewModel::class)
class MainActivity : AbsActivity<ActivityMainBinding, MainViewModel>() {

    private val mItemClickListener = object: OnItemClickListener<Country> {
        override fun onItemClicked(item: Country) {
            val args = Bundle().apply {
                putParcelable(Keys.COUNTRY_LIST_ITEM, item)
            }

            val countryItemDialogFrag = CountryDetailDialogFrag().apply { arguments = args }
            countryItemDialogFrag.show(supportFragmentManager, "tag")
        }
    }
    private val mResultsAdapter by lazy { SearchResultsAdapter(viewModel!!, mItemClickListener) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(toolbar)

        binding.vm = viewModel
        binding.lifecycleOwner = this

        with (rvResults) {
            addItemDecoration(DividerItemDecoration(this@MainActivity, DividerItemDecoration.HORIZONTAL))
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = mResultsAdapter
        }

        etSearch.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                filterList()

                with(etSearch.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager) {
                    hideSoftInputFromWindow(etSearch.windowToken, 0)
                }
                true
            } else false
        }
    }

    private fun filterList() {
        mResultsAdapter.filter(viewModel?.queryObservable?.get()!!)
    }

    override fun onStart() {
        super.onStart()
        viewModel?.let { vm ->
            vm.reObserve(vm.searchResultsSource, mSearchResultsObserver)
            vm.reObserve(vm.itemForUpdateSource, mItemForUpdateObserver)
        }
    }

    private val mSearchResultsObserver = Observer<Resource<List<Country>>> {
        when (it.status) {
            Resource.Status.SUCCESS -> {
                mResultsAdapter.addList(it.data!!)
            }
            Resource.Status.ERROR -> {
                val message = it.exception?.message
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private val mItemForUpdateObserver = Observer<Pair<Country, Int>> {
        mResultsAdapter.update(it.first, it.second)
    }
}

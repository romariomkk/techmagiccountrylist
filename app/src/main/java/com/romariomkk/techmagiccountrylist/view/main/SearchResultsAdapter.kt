package com.romariomkk.techmagiccountrylist.view.main

import android.view.View
import com.romariomkk.techmagiccountrylist.R
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import com.romariomkk.techmagiccountrylist.databinding.ItemResultBinding
import com.romariomkk.techmagiccountrylist.view.base.AbsFilterableAdapter
import com.romariomkk.techmagiccountrylist.view.base.AbsRVViewHolder
import com.romariomkk.techmagiccountrylist.view.base.OnItemClickListener

class SearchResultsAdapter(private val vm: MainViewModel, itemClickListener: OnItemClickListener<Country>)
    : AbsFilterableAdapter<Country, ItemResultBinding, SearchResultsAdapter.CountryListElementVH, String>(itemClickListener) {

    override fun provideLayoutId(viewType: Int) = R.layout.item_result

    override fun getViewHolder(view: View, viewType: Int) = CountryListElementVH(view, vm)

    override fun areItemsTheSame(oldItem: Country, newItem: Country) = oldItem.name == newItem.name

    override fun areContentTheSame(oldItem: Country, newItem: Country) = oldItem == newItem

    override fun isRuleActivated(whatToCompare: Country, withWhatToCompare: String): Boolean {
        return whatToCompare.name.contains(withWhatToCompare, true)
    }

    class CountryListElementVH(view: View, private val viewModel: MainViewModel) : AbsRVViewHolder<Country, ItemResultBinding>(view) {
        override fun bind(item: Country?, payloads: MutableList<Any>?) {
            super.bind(item, payloads)
            binding?.run {
                result = item
                position = adapterPosition
                vm = viewModel
                executePendingBindings()
            }
        }
    }
}
package com.romariomkk.techmagiccountrylist.view.main

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import com.romariomkk.techmagiccountrylist.core.domain.repo.contract.SearchRepo
import com.romariomkk.techmagiccountrylist.util.Resource
import com.romariomkk.techmagiccountrylist.view.base.AbsViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val searchRepo: SearchRepo
) : AbsViewModel() {

    val searchResultsSource = MutableLiveData<Resource<List<Country>>>()
    val itemForUpdateSource = MutableLiveData<Pair<Country, Int>>()
    val queryObservable = ObservableField<String>()

    override fun onAttached() {
        add(
            searchRepo.requestCountries()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { searchResultsSource.value = Resource.loading() }
                .doOnDispose { searchResultsSource.value = Resource.abort() }
                .subscribe({ countries ->
                    searchResultsSource.value = Resource.success(countries)
                }, {
                    it.printStackTrace()
                    searchResultsSource.value = Resource.error(it)
                })
        )
    }

    fun setFav(country: Country, position: Int) {
        add(
            searchRepo.setFav(country)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ updatedItem ->
                    itemForUpdateSource.value = Pair(updatedItem, position)
                }, { err ->
                    err.printStackTrace()
                })
        )
    }

}
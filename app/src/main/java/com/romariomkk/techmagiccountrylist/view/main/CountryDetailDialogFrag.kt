package com.romariomkk.techmagiccountrylist.view.main

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.romariomkk.techmagiccountrylist.R
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import com.romariomkk.techmagiccountrylist.databinding.FragmentCountryDetailBinding
import com.romariomkk.techmagiccountrylist.util.Keys

class CountryDetailDialogFrag: DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layoutView = LayoutInflater.from(context).inflate(R.layout.fragment_country_detail, null)
        val fragBinding = FragmentCountryDetailBinding.bind(layoutView)

        fragBinding.result = arguments?.getParcelable(Keys.COUNTRY_LIST_ITEM) as Country

        val dialog = AlertDialog.Builder(context!!).setView(fragBinding.root).create()
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }


}
package com.romariomkk.techmagiccountrylist.view.base

import androidx.databinding.ViewDataBinding
import java.util.*

abstract class AbsFilterableAdapter<T, DB : ViewDataBinding, VH : AbsRVViewHolder<T, DB>, W>(
    itemClickListener: OnItemClickListener<T>? = null
) : AbsRVAdapter<T, DB, VH>(itemClickListener),
    IFilterableAdapter<T, W> {

    private val unfilteredItems = LinkedList<T>()

    override val unfilteredData: List<T>
        get() = unfilteredItems

    override fun publishResults(newItems: List<T>) {
        updateItems(newItems)
    }

    override fun addList(newItems: List<T>) {
        super.addList(newItems)
        unfilteredItems.addAll(newItems)
    }

    override fun clear() {
        super.clear()
        unfilteredItems.clear()
    }
}
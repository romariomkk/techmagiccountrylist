package com.romariomkk.techmagiccountrylist.core.domain.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import io.reactivex.Single

@Dao
abstract class CountryDao {

    @Transaction
    open fun saveCountries(searchResults: List<Country>) {
        deleteAll()
        saveAll(searchResults)
    }

    @Query("DELETE FROM $TABLE_NAME")
    protected abstract fun deleteAll()

    @Insert
    protected abstract fun saveAll(searchResults: List<Country>)

    @Query("SELECT * FROM $TABLE_NAME")
    abstract fun getAll(): Single<List<Country>>

    @Query("UPDATE $TABLE_NAME SET isFav = :isFav WHERE name = :name")
    abstract fun setFav(name: String, isFav: Int)

    @Query("SELECT * FROM $TABLE_NAME WHERE name = :name")
    abstract fun getByName(name: String): Single<Country>

    companion object {
        const val TABLE_NAME = "countries"
    }
}
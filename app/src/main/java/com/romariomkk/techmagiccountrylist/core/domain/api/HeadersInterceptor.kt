package com.romariomkk.techmagiccountrylist.core.domain.api

import android.os.Build
import com.romariomkk.techmagiccountrylist.BuildConfig
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class HeadersInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(addHeaders(chain))
    }

    private fun addHeaders(chain: Interceptor.Chain): Request {
        val requestBuilder = chain.request().newBuilder()
        return requestBuilder
            .addHeader("x-rapidapi-host", "restcountries-v1.p.rapidapi.com")
            .addHeader("x-rapidapi-Key", BuildConfig.API_KEY)
            .build()
    }
}
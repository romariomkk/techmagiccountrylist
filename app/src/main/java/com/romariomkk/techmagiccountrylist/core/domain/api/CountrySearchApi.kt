package com.romariomkk.techmagiccountrylist.core.domain.api

import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import io.reactivex.Single
import retrofit2.http.GET

interface CountrySearchApi {

    @GET("all?endpoint=53aa5a08e4b0a705fcc323a6")
    fun searchAll() : Single<List<Country>>
//
//    @GET("rest-countries-v1/name/{name}?endpoint=53aa5a08e4b0a705fcc323a6")
//    fun searchWithQuery(@Path("name") name: String?) : Single<List<Country>>
}
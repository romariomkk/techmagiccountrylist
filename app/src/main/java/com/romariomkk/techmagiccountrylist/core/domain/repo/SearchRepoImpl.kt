package com.romariomkk.techmagiccountrylist.core.domain.repo

import com.romariomkk.techmagiccountrylist.core.domain.api.CountrySearchApi
import com.romariomkk.techmagiccountrylist.core.domain.db.CountryDao
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import com.romariomkk.techmagiccountrylist.core.domain.repo.contract.SearchRepo
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SearchRepoImpl @Inject constructor(
    private val api: CountrySearchApi,
    private val countryDao: CountryDao
) : SearchRepo {

    override fun requestCountries(): Single<List<Country>> {
        return countryDao.getAll()
            .filter { it.isNotEmpty() }
            .switchIfEmpty(api.searchAll()
                .flatMap { list ->
                    countryDao.saveCountries(list)
                    Single.just(list)
                })
            .subscribeOn(Schedulers.io())
    }

    override fun setFav(country: Country): Single<Country> {
        return Completable.fromAction { countryDao.setFav(country.name, if (country.isFav) 0 else 1) }
            .andThen(countryDao.getByName(country.name))
            .subscribeOn(Schedulers.io())
    }

    companion object {
        val TAG: String = SearchRepoImpl::class.java.simpleName
    }
}
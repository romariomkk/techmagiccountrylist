package com.romariomkk.techmagiccountrylist.core.domain.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country

@Database(entities = [Country::class], version = 1, exportSchema = false)
abstract class CountryAppDB: RoomDatabase() {

    abstract fun countryDao(): CountryDao

    companion object {
        const val DB_FILE_NAME = "countryapp.db"
    }
}
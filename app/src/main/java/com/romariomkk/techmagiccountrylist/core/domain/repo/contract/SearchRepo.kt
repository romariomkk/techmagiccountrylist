package com.romariomkk.techmagiccountrylist.core.domain.repo.contract

import com.romariomkk.techmagiccountrylist.core.domain.pojo.Country
import io.reactivex.Single

interface SearchRepo {
    fun requestCountries(): Single<List<Country>>
    fun setFav(country: Country): Single<Country>
}
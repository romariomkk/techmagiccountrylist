package com.romariomkk.techmagiccountrylist.core.domain.pojo

import android.os.Parcelable
import androidx.room.Entity
import com.romariomkk.techmagiccountrylist.core.domain.db.CountryDao
import kotlinx.android.parcel.Parcelize

@Entity(tableName = CountryDao.TABLE_NAME, primaryKeys = ["name"])
@Parcelize
data class Country (
    val name: String,
    val alpha3Code: String,
    val capital: String,
    val region: String,
    val subregion: String,
    val population: Long,
    val area: Double? = null,
    val numericCode: String? = null,
    val isFav: Boolean = false
): Parcelable
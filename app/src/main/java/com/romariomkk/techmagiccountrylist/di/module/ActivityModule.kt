package com.romariomkk.techmagiccountrylist.di.module

import com.romariomkk.techmagiccountrylist.view.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

}
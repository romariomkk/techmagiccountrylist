package com.romariomkk.techmagiccountrylist.di.module

import androidx.room.Room
import com.romariomkk.techmagiccountrylist.TechMagicCountryApp
import com.romariomkk.techmagiccountrylist.core.domain.db.CountryAppDB
import com.romariomkk.techmagiccountrylist.core.domain.db.CountryDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(app: TechMagicCountryApp) =
        Room.databaseBuilder(app, CountryAppDB::class.java, CountryAppDB.DB_FILE_NAME)
            .fallbackToDestructiveMigration()
            .build()

    @Provides
    @Singleton
    fun provideContactDao(db: CountryAppDB): CountryDao = db.countryDao()
}
package com.romariomkk.techmagiccountrylist.di.module

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.romariomkk.techmagiccountrylist.TechMagicCountryApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [
    DatabaseModule::class,
    NetworkModule::class,
    RepositoryModule::class])
internal class AppModule {

    @Singleton
    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideApp(application: Application): TechMagicCountryApp = application as TechMagicCountryApp

    @Singleton
    @Provides
    fun provideContext(application: Application): Context = application.applicationContext

}

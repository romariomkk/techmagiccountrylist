package com.romariomkk.techmagiccountrylist.di.module

import com.romariomkk.techmagiccountrylist.core.domain.repo.SearchRepoImpl
import com.romariomkk.techmagiccountrylist.core.domain.repo.contract.SearchRepo
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindSearchRepository(repository: SearchRepoImpl): SearchRepo
}
package com.romariomkk.techmagiccountrylist.di

import android.app.Application
import com.romariomkk.techmagiccountrylist.TechMagicCountryApp
import com.romariomkk.techmagiccountrylist.di.module.ActivityModule
import com.romariomkk.techmagiccountrylist.di.module.AppModule
import com.romariomkk.techmagiccountrylist.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            ActivityModule::class,
            ViewModelModule::class,
            AppModule::class
        ])
interface AppComponent : AndroidInjector<TechMagicCountryApp> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(satisfyerApp: TechMagicCountryApp)
}
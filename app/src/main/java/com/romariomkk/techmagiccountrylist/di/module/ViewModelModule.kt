package com.romariomkk.techmagiccountrylist.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.romariomkk.techmagiccountrylist.di.ViewModelKey
import com.romariomkk.techmagiccountrylist.di.ViewModelProviderFactory
import com.romariomkk.techmagiccountrylist.view.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}
package com.romariomkk.techmagiccountrylist

import android.content.Context
import com.facebook.stetho.Stetho
import com.romariomkk.techmagiccountrylist.di.AppComponent
import com.romariomkk.techmagiccountrylist.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class TechMagicCountryApp: DaggerApplication() {

    private lateinit var androidInjector: AndroidInjector<out DaggerApplication>

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        INSTANCE = this
        androidInjector = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this)
        }
    }

    companion object {
        private var INSTANCE: TechMagicCountryApp? = null
        @JvmStatic
        fun get(): TechMagicCountryApp = INSTANCE!!

        @JvmStatic
        public fun getAppComponent(): AppComponent {
            return TechMagicCountryApp.get().androidInjector as AppComponent
        }
    }

    public override fun applicationInjector(): AndroidInjector<out DaggerApplication> = androidInjector
}